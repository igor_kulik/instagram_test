//
//  User.h
//  instagram_test
//
//  Created by Igor on 4/22/15.
//  Copyright (c) 2015 Igor. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface User : NSManagedObject

@property (nonatomic, retain) NSString * username;
@property (nonatomic, retain) NSString * full_name;
@property (nonatomic, retain) NSString * user_id;
@property (nonatomic, retain) NSSet *usersMedia;
@end

@interface User (CoreDataGeneratedAccessors)

- (void)addUsersMediaObject:(NSManagedObject *)value;
- (void)removeUsersMediaObject:(NSManagedObject *)value;
- (void)addUsersMedia:(NSSet *)values;
- (void)removeUsersMedia:(NSSet *)values;

@end
