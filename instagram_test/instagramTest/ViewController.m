//
//  ViewController.m
//  instagramTest
//
//  Created by Igor on 4/8/15.
//  Copyright (c) 2015 Igor. All rights reserved.
//

#import "ViewController.h"
#import "JSON.h"
#import "URLRequestCenter.h"
@interface ViewController ()
@property (strong, nonatomic) IBOutlet UITextView *text;
@property (strong, nonatomic) IBOutlet UISearchBar *searchBar;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    URLRequestCenter *requestCenter = [[URLRequestCenter alloc] init];
    [self parseFeed:[requestCenter  getFeed]];
}
- (void) parseFeed:(NSDictionary *)feedData {
    for( NSDictionary *data in feedData[@"data"])
    {
        NSLog(@"url:%@", [NSString stringWithFormat:@"%@",data[@"images"][@"thumbnail"][@"url"]]);
        NSLog(@"username:%@", [NSString stringWithFormat:@"%@",data[@"user"][@"username"]]);
        NSLog(@"created_time:%@", [NSString stringWithFormat:@"%@",data[@"created_time"]]);
        NSDictionary *caption = data[@"caption"];
        if(caption != (NSDictionary *)[NSNull null])
            NSLog(@"text:%@", [NSString stringWithFormat:@"%@",caption[@"text"]]);
        NSLog(@"\n\n\n");
    }
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}
@end
