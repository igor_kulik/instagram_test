//
//  URLRequestCenter.h
//  instagramTest
//
//  Created by Igor on 4/9/15.
//  Copyright (c) 2015 Igor. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface URLRequestCenter : NSObject<NSURLConnectionDelegate>
{
    NSMutableData *_responseData;
    NSDictionary *_receivedData;
    int counter;
}
- (void) count;
- (NSDictionary *) getFeed;
- (NSDictionary *) search:(NSString *)username;
+ (URLRequestCenter *) sharedInstance;
@end