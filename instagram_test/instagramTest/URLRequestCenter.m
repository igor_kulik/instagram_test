//
//  URLRequestCenter.m
//  instagramTest
//
//  Created by Igor on 4/9/15.
//  Copyright (c) 2015 Igor. All rights reserved.
//

#import "URLRequestCenter.h"
#import "JSON.h"
@implementation URLRequestCenter

static URLRequestCenter *sSingleton = nil;

enum requests {
    feed,
    search,
    map,
    favorites
};
int requestType;
NSString *client_ID = @"f9abf80f193343dfafd665d879ffe8e2";

//NSDictionary *receivedData;
#pragma mark -feed

- (NSDictionary *) getFeed {
    NSURLRequest * feedRequest = [NSURLRequest requestWithURL:
                                  [NSURL URLWithString:
                                   [NSString stringWithFormat:@"https://api.instagram.com/v1/media/popular?client_id=%@", client_ID]]];
    [self sendURLRequest:feedRequest];
    requestType = feed;
    return _receivedData;
    
}

- (void) parseFeed:(NSDictionary *)feedData {
    for( NSDictionary *data in feedData[@"data"])
    {
        NSLog(@"url:%@", [NSString stringWithFormat:@"%@",data[@"images"][@"thumbnail"][@"url"]]);
        NSLog(@"username:%@", [NSString stringWithFormat:@"%@",data[@"user"][@"username"]]);
        NSLog(@"created_time:%@", [NSString stringWithFormat:@"%@",data[@"created_time"]]);
        NSDictionary *caption = data[@"caption"];
        if(caption != (NSDictionary *)[NSNull null])
            NSLog(@"text:%@", [NSString stringWithFormat:@"%@",caption[@"text"]]);
        NSLog(@"\n\n\n");
    }
}

#pragma mark -search
- (NSDictionary *) search:(NSString *)username {
    NSURLRequest * searchRequest = [NSURLRequest requestWithURL:
                                    [NSURL URLWithString:
                                     [NSString stringWithFormat:@"https://api.instagram.com/v1/users/search?q=%@&client_id=%@", username, client_ID]]];
    [self sendURLRequest:searchRequest];
    requestType = search;
    return _receivedData;
}

- (void) parseSearch:(NSDictionary *)searchData {
    for( NSDictionary *user in searchData[@"data"])
        NSLog(@"username:%@\n\n", [NSString stringWithFormat:@"%@",user[@"username"]]);
    
}

#pragma mark -connection
- (void) sendURLRequest: (NSURLRequest *)request {
    (void)[[NSURLConnection alloc]  initWithRequest:request
                                           delegate:self
                                   startImmediately:true];
}

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response {
    _responseData = [[NSMutableData alloc] init];
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data {
    [_responseData appendData:data];
}

- (NSCachedURLResponse *)connection:(NSURLConnection *)connection
                  willCacheResponse:(NSCachedURLResponse*)cachedResponse {
    return nil;
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    NSString *response = [[NSString alloc] initWithData:_responseData encoding:NSUTF8StringEncoding];
    SBJsonParser *jResponse = [[SBJsonParser alloc]init];
    NSDictionary *tokenData = [jResponse objectWithString:response];
        switch(requestType) {
            case feed:
                [self parseFeed:tokenData];
            case search:
                [self parseSearch:tokenData];
            case map:
    
            case favorites:
    
            default:
                break;
        };
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error {
}




#pragma mark -singleton
+ (URLRequestCenter *) sharedInstance
{
    @synchronized(self)
    {
        if(!sSingleton)
            sSingleton = [[super allocWithZone:NULL] init];
    }
    return sSingleton;
}

+ (id) allocWithZone:(NSZone *)zone
{
    return [[self sharedInstance] retain];
}

- (id) copyWithZone:(NSZone*)zone
{
    return self;
}

- (id) retain
{
    return self;
}

- (NSUInteger) retainCount
{
    return NSUIntegerMax;
}

- (void) release { }

- (id) autorelease
{
    return self;
}

- (void) count
{
    NSLog(@"Counter: %d", ++counter);
}

@end
