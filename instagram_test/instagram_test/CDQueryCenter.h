//
//  CDQueryCenter.h
//  instagram_test
//
//  Created by Igor on 4/21/15.
//  Copyright (c) 2015 Igor. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import <UIKit/UIKit.h>
#import "Media.h"
#import "User.h"
#import "Comment.h"
@interface CDQueryCenter : NSObject
- (void) addMediaToFavorites: (Media *) media;
- (void) removeMediaFromFavorites: (Media *) media;
- (void) addUserToFavorites: (User *) user;
- (void) removeUserFromFavorites: (User *) user;
- (void) addComment:(Comment *) comment forMedia:(Media *) media;
- (void) deleteComment: (Comment *) comment;
- (bool) checkIfMediaIsFavorite: (NSString *) media_id;
- (bool) checkIfUserIsFavorite: (NSString *) user_id;
- (NSArray *) fetchFavoriteMedia;               //set of Media objects
- (NSArray *) fetchFavoriteUsers;               //set of User objects
- (NSSet *) fetchComments: (Media *) media;     //set of Comment objects
+ (CDQueryCenter *) sharedInstance;
@end
