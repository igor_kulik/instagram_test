
//
//  FeedTableViewController.m
//  instagram_test
//
//  Created by Igor on 4/10/15.
//  Copyright (c) 2015 Igor. All rights reserved.
//

#import "FeedTableViewController.h"
#import "FeedTableViewCell.h"
#import "URLRequestCenter.h"
#import "MediaViewController.h"
#import "Media.h"
@interface FeedTableViewController ()
@property (nonatomic, strong) NSMutableArray *tableData;

@end

@implementation FeedTableViewController
UIAlertView *alertView;
#pragma mark - Properties instantiation
- (NSMutableArray *) tableData
{
    if(!_tableData)
        _tableData = [[NSMutableArray alloc] initWithCapacity: 5];
    return _tableData;
}

#pragma mark - View controller lifecycle
- (void)viewDidLoad
{
    [super viewDidLoad];
    [[URLRequestCenter sharedInstance] getFeedWithCompletion: ^(id response) { [self processFeed: response]; }
                                                     failure: ^(id error){ [self processError: error]; }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - Process received data
- (void) processFeed: (NSDictionary *)feedData
{
    for(NSDictionary *media in feedData[@"data"])
    {
        Media *newMedia = [[Media alloc] init];
        newMedia.media_id = media[@"id"];
        newMedia.username = media[@"user"][@"username"];
        newMedia.date = media[@"created_time"];
        newMedia.type = media[@"type"];
        NSString *mediaDirectory = [newMedia.type isEqualToString:@"image"] ? @"images": @"videos";
        newMedia.url = media[mediaDirectory][@"standard_resolution"][@"url"];
        newMedia.mediaImage = [[NSData alloc] initWithContentsOfURL: [NSURL URLWithString: media[@"images"][@"standard_resolution"][@"url"]]];
        NSDictionary *caption = media[@"caption"];
        if(caption != (NSDictionary *)[NSNull null])
            newMedia.caption = caption[@"text"];
        else
            newMedia.caption = @"";
        [self.tableData addObject:newMedia];
    }
    [self.tableView reloadData];
}

- (void) processError: (NSDictionary *)error
{
    NSString *pdata = [NSString stringWithFormat:@"error_message: %@\ncode: %@\nerror_type: %@",
                       error[@"error_message"],
                       error[@"code"],
                       error[@"error_type"]];
    
    alertView = [[UIAlertView alloc] initWithTitle: @"Error"
                                           message: pdata
                                          delegate: self
                                 cancelButtonTitle: @"OK"
                                 otherButtonTitles: @"Reconnect", nil];
    [alertView show];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if ([[alertView buttonTitleAtIndex: buttonIndex]  isEqual: @"Reconnect"])
    {
        NSLog(@"reconnected");
        [self viewDidLoad];
    }
}

#pragma mark - Table view data source
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView
 numberOfRowsInSection: (NSInteger)section
{
    return [self.tableData count];
}

- (FeedTableViewCell *)tableView:(UITableView *)tableView
           cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    //Create cell
    static NSString *CellIdentifier = @"feedTableCell";
    FeedTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier: CellIdentifier];
    if (!cell)
        cell = [[FeedTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    //Configure the cell
    cell.accessoryType = UITableViewCellAccessoryNone;
    Media *newMedia = self.tableData[indexPath.row];
    cell.descriptionLabel.text = newMedia.caption;
    cell.usernameLabel.text = newMedia.username;
    cell.dateLabel.text = newMedia.date;
    UIImage *image = [[UIImage alloc] initWithData:newMedia.mediaImage];
    [cell.mediaImage setImage: image];
    
    return cell;
}

#pragma mark - Transition to media screen
- (void)tableView:(UITableView *)tableView
didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
        MediaViewController *mediaViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"MediaViewController_ID"];
        [self.navigationController pushViewController:mediaViewController
                                             animated:YES];
        mediaViewController.mediaData = [self.tableData objectAtIndex:indexPath.row];
}


@end
