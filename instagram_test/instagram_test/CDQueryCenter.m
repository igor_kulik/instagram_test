//
//  CDQueryCenter.m
//  instagram_test
//
//  Created by Igor on 4/21/15.
//  Copyright (c) 2015 Igor. All rights reserved.
//

#import "CDQueryCenter.h"

@implementation CDQueryCenter
static CDQueryCenter *sSingleton = nil;
- (NSManagedObjectContext *)managedObjectContext
{
    NSManagedObjectContext *context = nil;
    id delegate = [[UIApplication sharedApplication] delegate];
    if ([delegate performSelector:@selector(managedObjectContext)]) {
        context = [delegate managedObjectContext];
    }
    return context;
}

#pragma mark - Add/remove favorite media
- (void) addMediaToFavorites: (Media *) media
{
    NSManagedObject *newMedia = [NSEntityDescription insertNewObjectForEntityForName:@"Media"
                                                              inManagedObjectContext:self.managedObjectContext];
    [newMedia setValue:media.media_id
                forKey:@"media_id"];
    [newMedia setValue:media.type
                forKey:@"type"];
    [newMedia setValue:media.url
                forKey:@"url"];
    [newMedia setValue:media.username
                forKey:@"username"];
    [newMedia setValue:media.caption
                forKey:@"caption"];
    [newMedia setValue:media.date
                forKey:@"date"];
    [newMedia setValue:media.mediaImage
                forKey:@"preview"];
    NSError *error;
    if (![self.managedObjectContext save:&error])
        NSLog(@"Unresolved error %@, %@", error, [error localizedDescription]);
}

- (void) removeMediaFromFavorites: (Media *) media
{
    NSManagedObject *newMedia = [[CDQueryCenter sharedInstance]fetchMedia:media.media_id];
    [self.managedObjectContext deleteObject:newMedia];
    NSError *error = nil;
    if (![self.managedObjectContext save:&error])
        NSLog(@"Can't delete! %@ %@", error, [error localizedDescription]);
}

#pragma mark - Add/remove favorite user
- (void) addUserToFavorites: (User *) user
{
    NSManagedObject *newUser = [NSEntityDescription insertNewObjectForEntityForName:@"User"
                                                             inManagedObjectContext:self.managedObjectContext];
    [newUser setValue:user.username
               forKey:@"username"];
    [newUser setValue:user.full_name
               forKey:@"full_name"];
    [newUser setValue:user.user_id
               forKey:@"user_id"];
    NSError *error;
    if (![self.managedObjectContext save:&error])
        NSLog(@"Unresolved error %@, %@", error, [error localizedDescription]);
}

- (void) removeUserFromFavorites: (User *) user
{
    NSManagedObject *newUser = [self fetchUser:user.user_id];
    [self.managedObjectContext deleteObject:newUser];
    NSError *error = nil;
    if (![self.managedObjectContext save:&error])
        NSLog(@"Can't delete! %@ %@", error, [error localizedDescription]);
}

#pragma mark - Add/remove comment
- (void) addComment:(Comment *) comment
           forMedia:(Media *) media
{
    NSManagedObject *newComment = [NSEntityDescription insertNewObjectForEntityForName:@"Comment"
                                                                inManagedObjectContext:self.managedObjectContext];
    NSManagedObject *newMedia = [[CDQueryCenter sharedInstance] fetchMedia:media.media_id];
    [newComment setValue:comment.text
                  forKey:@"text"];
    [newComment setValue:newMedia
                  forKey:@"media"];
    NSError *error = nil;
    if (![self.managedObjectContext save:&error])
        NSLog(@"Can't save! %@ %@", error, [error localizedDescription]);
}

- (void) deleteComment: (Comment *) comment
{
    NSManagedObject *newComment = [[CDQueryCenter sharedInstance] fetchComment:comment.text];
    NSManagedObject *newMedia = [newComment valueForKey:@"media"];
    NSMutableSet *comments = [newMedia valueForKey:@"commentsForMedia"];
    [comments removeObject:newComment];
    [self.managedObjectContext deleteObject:newComment];
    NSError *error = nil;
    if (![self.managedObjectContext save:&error])
        NSLog(@"Can't delete! %@ %@", error, [error localizedDescription]);
}

#pragma mark - Check if favorite
- (bool) checkIfMediaIsFavorite: (NSString *) media_id
{
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Media"
                                              inManagedObjectContext:self.managedObjectContext];
    [request setEntity:entity];
    NSPredicate *predicate = [NSPredicate predicateWithFormat: @"media_id == %@", media_id];
    [request setPredicate:predicate];
    NSError *error = nil;
    NSArray *result = [self.managedObjectContext executeFetchRequest:request error:&error];
    [request release];
    if ((result) && ([result count] == 1) && (!error))
        return YES;
    else
        return NO;
}

- (bool) checkIfUserIsFavorite: (NSString *) user_id
{
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"User"
                                              inManagedObjectContext:self.managedObjectContext];
    [request setEntity:entity];
    NSPredicate *predicate = [NSPredicate predicateWithFormat: @"user_id == %@", user_id];
    [request setPredicate:predicate];
    NSError *error = nil;
    NSArray *result = [self.managedObjectContext executeFetchRequest:request error:&error];
    [request release];
    if ((result) && ([result count] == 1) && (!error))
        return YES;
    else
        return NO;
}

#pragma mark - Fetch favorite media/user and comments
- (NSArray *) fetchFavoriteMedia
{
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Media"
                                              inManagedObjectContext:self.managedObjectContext];
    [request setEntity:entity];
    NSError *error = nil;
    NSArray *response = [self.managedObjectContext executeFetchRequest:request error:&error];
    NSMutableArray *result = [[NSMutableArray alloc] initWithCapacity:1];
    for (id CDMedia in response)
    {
        Media *newMedia = [[Media alloc] init];
        newMedia.media_id = [CDMedia valueForKey:@"media_id"];
        newMedia.type = [CDMedia valueForKey:@"type"];
        newMedia.url = [CDMedia valueForKey:@"url"];
        newMedia.username = [CDMedia valueForKey:@"username"];
        newMedia.caption = [CDMedia valueForKey:@"caption"];
        newMedia.date = [CDMedia valueForKey:@"date"];
        newMedia.mediaImage = [CDMedia valueForKey:@"preview"];
        [result addObject:newMedia];
    }
    return result;
}

- (void) checkCommentsDeletion
{
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Comment"
                                              inManagedObjectContext:self.managedObjectContext];
    [request setEntity:entity];
    NSError *error = nil;
    NSArray *result = [self.managedObjectContext executeFetchRequest:request error:&error];
    [request release];
    if ((result) && ([result count]) && (!error))
    {
        for (id comment in result)
            NSLog(@"%@", [comment valueForKey:@"text"]);
    }
    else
        NSLog(@"ok");

}
- (NSArray *) fetchFavoriteUsers
{
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"User"
                                              inManagedObjectContext:self.managedObjectContext];
    [request setEntity:entity];
    NSError *error = nil;
    NSArray *response = [self.managedObjectContext executeFetchRequest:request error:&error];
    NSMutableArray *result = [[NSMutableArray alloc] initWithCapacity:1];
    for(id CDUser in response)
    {
        User *newUser = [[User alloc] init];
        newUser.username = [CDUser valueForKey:@"username"];
        newUser.full_name = [CDUser valueForKey:@"full_name"];
        newUser.user_id = [CDUser valueForKey:@"user_id"];
        [result addObject:newUser];
    }
    return result;
}

- (NSSet *) fetchComments: (Media *) media
{
    NSManagedObject *newMedia = [[CDQueryCenter sharedInstance] fetchMedia:media.media_id];
    NSSet *comments = [newMedia valueForKey:@"commentsForMedia"];
    return comments;
}

#pragma mark - Private fetching queries
- (NSManagedObject *) fetchMedia: (NSString *) media_id
{
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Media"
                                              inManagedObjectContext:self.managedObjectContext];
    [request setEntity:entity];
    NSPredicate *predicate = [NSPredicate predicateWithFormat: @"media_id == %@", media_id];
    [request setPredicate:predicate];
    NSError *error = nil;
    NSArray *result = [self.managedObjectContext executeFetchRequest:request error:&error];
    [request release];
    if ((result) && ([result count] == 1) && (!error))
        return result[0];
    else
        return nil;
}

- (NSManagedObject *) fetchUser: (NSString *) user_id
{
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"User"
                                              inManagedObjectContext:self.managedObjectContext];
    [request setEntity:entity];
    NSPredicate *predicate = [NSPredicate predicateWithFormat: @"user_id == %@", user_id];
    [request setPredicate:predicate];
    NSError *error = nil;
    NSArray *result = [self.managedObjectContext executeFetchRequest:request error:&error];
    [request release];
    if ((result) && ([result count]) && (!error))
        return result[0];
    else
        return nil;
}

- (NSManagedObject *) fetchComment: (NSString *) text
{
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Comment"
                                              inManagedObjectContext:self.managedObjectContext];
    [request setEntity:entity];
    NSPredicate *predicate = [NSPredicate predicateWithFormat: @"text == %@", text];
    [request setPredicate:predicate];
    NSError *error = nil;
    NSArray *result = [self.managedObjectContext executeFetchRequest:request error:&error];
    [request release];
    if ((result) && ([result count]) && (!error))
        return result[0];
    else
        return nil;
}

#pragma mark - Singleton properties
+ (CDQueryCenter *) sharedInstance
{
    @synchronized(self)
    {
        if(!sSingleton)
            sSingleton = [[super allocWithZone:NULL] init];
    }
    return sSingleton;
}

+ (id) allocWithZone:(NSZone *)zone
{
    return [[self sharedInstance] retain];
}

- (id) copyWithZone:(NSZone*)zone
{
    return self;
}

- (id) retain
{
    return self;
}

- (NSUInteger) retainCount
{
    return NSUIntegerMax;
}

- (void) release { }

- (id) autorelease
{
    return self;
}
@end
