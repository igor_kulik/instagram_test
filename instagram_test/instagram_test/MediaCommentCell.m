//
//  MediaCommentCell.m
//  instagram_test
//
//  Created by Igor on 4/21/15.
//  Copyright (c) 2015 Igor. All rights reserved.
//

#import "MediaCommentCell.h"

@implementation MediaCommentCell

- (void)awakeFromNib
{

}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    self.commentLabel.frame = CGRectMake(0,
                                         0,
                                         self.contentView.frame.size.width - self.contentView.frame.size.height,
                                         self.contentView.frame.size.height);

    self.removeCommentButton.frame = CGRectMake(self.contentView.frame.size.width - self.contentView.frame.size.height,
                                                0,
                                                self.contentView.frame.size.height,
                                                self.contentView.frame.size.height);
}

@end
