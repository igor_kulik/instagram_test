//
//  User.h
//  instagram_test
//
//  Created by Igor on 4/22/15.
//  Copyright (c) 2015 Igor. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface User : NSObject
@property (nonatomic, retain) NSString * username;
@property (nonatomic, retain) NSString * full_name;
@property (nonatomic, retain) NSString * user_id;
@end
