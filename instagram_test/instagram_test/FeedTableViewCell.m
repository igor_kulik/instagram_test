//
//  FeedTableViewCell.m
//  instagram_test
//
//  Created by Igor on 4/10/15.
//  Copyright (c) 2015 Igor. All rights reserved.
//

#import "FeedTableViewCell.h"

@implementation FeedTableViewCell

- (void)awakeFromNib {

}

- (instancetype) initWithStyle:(UITableViewCellStyle)style
               reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle: style
                reuseIdentifier: reuseIdentifier];
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    CGRect frameImage;
    frameImage.size.width = self.contentView.frame.size.width - 40;
    frameImage.origin.x = 20;
    frameImage.size.height = frameImage.size.width;
    self.mediaImage.frame = frameImage;
    self.mediaImage.center = self.contentView.center;

    CGRect frameDesc = self.descriptionLabel.frame;
    frameDesc.size.width = self.mediaImage.frame.size.width * 0.595;
    frameDesc.origin.x = self.mediaImage.frame.origin.x;

    CGRect frameDate = self.dateLabel.frame;
    frameDate.origin.x = self.mediaImage.frame.origin.x + self.mediaImage.frame.size.width * 0.6;
    frameDate.size.width = self.mediaImage.frame.size.width * 0.4;
    self.dateLabel.frame = frameDate;

    self.descriptionLabel.frame = frameDesc;
    self.descriptionLabel.adjustsFontSizeToFitWidth = YES;
    self.descriptionLabel.minimumScaleFactor = 0.8;
}

@end
