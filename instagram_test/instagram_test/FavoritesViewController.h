//
//  FavoritesViewController.h
//  instagram_test
//
//  Created by Igor on 4/20/15.
//  Copyright (c) 2015 Igor. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FavoritesViewController : UIViewController <UITableViewDelegate, UITableViewDataSource>
{
    UISegmentedControl *segmentedControl;
    UITableView *favoritesTable;
}
@end
