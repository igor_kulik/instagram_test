//
//  Media.h
//  instagram_test
//
//  Created by Igor on 4/22/15.
//  Copyright (c) 2015 Igor. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Media : NSObject
@property (strong, nonatomic) NSString * media_id;
@property (strong, nonatomic) NSString * type;
@property (strong, nonatomic) NSString * url;
@property (strong, nonatomic) NSString * username;
@property (strong, nonatomic) NSString * caption;
@property (strong, nonatomic) NSString * date;
@property (strong, nonatomic) NSData * mediaImage;
@end
