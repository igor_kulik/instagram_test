//
//  FavoriteMediaTableViewCell.h
//  instagram_test
//
//  Created by Igor on 4/20/15.
//  Copyright (c) 2015 Igor. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FavoriteMediaTableViewCell : UITableViewCell
@property (strong, nonatomic) UILabel *usernameLabel;
@property (strong, nonatomic) UILabel *dateLabel;
@property (strong, nonatomic) UILabel *descriptionLabel;
@property (strong, nonatomic) UIImageView *mediaImage;
@end
