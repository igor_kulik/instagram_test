//
//  MediaCommentCell.h
//  instagram_test
//
//  Created by Igor on 4/21/15.
//  Copyright (c) 2015 Igor. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MediaCommentCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UIButton *removeCommentButton;
@property (strong, nonatomic) IBOutlet UILabel *commentLabel;
@end
