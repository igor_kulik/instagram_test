//
//  SearchTableViewController.h
//  instagram_test
//
//  Created by Igor on 4/9/15.
//  Copyright (c) 2015 Igor. All rights reserved.
//

#import <UIKit/UIKit.h>
@interface SearchTableViewController : UIViewController<UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate, UIScrollViewDelegate>
@property (strong, nonatomic) IBOutlet UISearchBar *searchBar;
@property (strong, nonatomic) IBOutlet UITableView *tableView;
@end
