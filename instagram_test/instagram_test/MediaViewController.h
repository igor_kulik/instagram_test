//
//  MediaViewController.h
//  instagram_test
//
//  Created by Igor on 4/15/15.
//  Copyright (c) 2015 Igor. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>
#import <MediaPlayer/MediaPlayer.h>
#import "Media.h"
@interface MediaViewController : UIViewController <UITableViewDelegate, UITableViewDataSource, UIScrollViewDelegate, UITextFieldDelegate>
@property (strong, nonatomic) Media *mediaData;
@property (strong, nonatomic) MPMoviePlayerViewController *moviePlayerController;
@property (strong, nonatomic) UIScrollView *scrollView;
@property (strong, nonatomic) UIImageView *imagePreview;

@property (strong, nonatomic) UIButton *addCommentButton;
@property (strong, nonatomic) UITextField *commentTextField;
@property (strong, nonatomic) IBOutlet UITableView *commentsTableView;
@property (strong, nonatomic) IBOutlet UIBarButtonItem *favButton;
@property (nonatomic) BOOL isFavorite;
@end
