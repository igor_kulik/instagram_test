//
//  Comment.h
//  instagram_test
//
//  Created by Igor on 4/22/15.
//  Copyright (c) 2015 Igor. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Comment : NSObject
@property (nonatomic, retain) NSString * text;
@end
