//
//  MapViewController.m
//  instagram_test
//
//  Created by Igor on 4/17/15.
//  Copyright (c) 2015 Igor. All rights reserved.
//

#import "MapViewController.h"
#import "MediaViewController.h"
#import "Media.h"
@interface MapViewController ()
@property (strong, nonatomic) IBOutlet MKMapView *mapView;
@property (strong, nonatomic) NSMutableArray *placemarksData;
@end

@implementation MapViewController
{
    CLLocationManager *locationManager;
}
#define IS_OS_8_OR_LATER ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0)

#pragma mark - View controller lifecycle
- (void)viewDidLoad
{
    [super viewDidLoad];
    self.mapView.delegate = self;
    self.mapView.mapType = MKMapTypeStandard;
    locationManager = [[CLLocationManager alloc] init];
    locationManager.delegate = self;
    locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    if(IS_OS_8_OR_LATER)
    {
        [locationManager requestWhenInUseAuthorization];
        [locationManager requestAlwaysAuthorization];
    }
    [locationManager startUpdatingLocation];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

#pragma mark - Process received data
- (void) processResponse: (NSDictionary *)responseData
{
    self.placemarksData = [NSMutableArray arrayWithArray: responseData[@"data"]];
    for(NSDictionary *media in responseData[@"data"])
    {
        //Coordinates
        CLLocationDegrees lat = [media[@"location"][@"latitude"] doubleValue];
        CLLocationDegrees lng = [media[@"location"][@"longitude"] doubleValue];
        CLLocationCoordinate2D locationCoords = CLLocationCoordinate2DMake(lat, lng);
        MKPointAnnotation *annotation = [[MKPointAnnotation alloc] init];
        //Date
        NSTimeInterval interval = [media[@"created_time"] doubleValue];
        NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"hh:mm dd.MM.yy"];
        NSString *dateString = [dateFormatter stringFromDate:[NSDate dateWithTimeIntervalSince1970: interval]];
        //Configure the annotation
        annotation.coordinate = locationCoords;
        annotation.title = media[@"user"][@"username"];
        annotation.subtitle = dateString;
        
        [self.mapView addAnnotation: annotation];
    }
}

- (void) processError: (NSDictionary *)error
{
    NSString *pdata = [NSString stringWithFormat:@"error_message: %@\ncode: %@\nerror_type: %@",
                       error[@"error_message"],
                       error[@"code"],
                       error[@"error_type"]];
    
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle: @"Error"
                                                        message: pdata
                                                       delegate: self
                                              cancelButtonTitle: @"OK"
                                              otherButtonTitles: nil];
    [alertView show];
}

#pragma mark - Annotation methods
- (MKAnnotationView *)mapView:(MKMapView *)mapView
            viewForAnnotation:(id <MKAnnotation>)annotation
{
    // If it's the user location, just return nil.
    if ([annotation isKindOfClass:[MKUserLocation class]])
        return nil;
    // Handle any custom annotations.
    if ([annotation isKindOfClass:[MKPointAnnotation class]])
    {
    MKPinAnnotationView *annotationView =
        (MKPinAnnotationView *)[mapView dequeueReusableAnnotationViewWithIdentifier:@"Annotation"];
    if(!annotationView)
    {
        annotationView = [[MKPinAnnotationView alloc] initWithAnnotation:annotation
                                                      reuseIdentifier:@"Annotation"];
        UIButton *rightButton = [UIButton buttonWithType: UIButtonTypeCustom];
        rightButton.frame = CGRectMake(0, 0, 48, 48);
        [rightButton setBackgroundImage: [UIImage imageNamed: @"arrow"]
                               forState: UIControlStateNormal];
        annotationView.rightCalloutAccessoryView = rightButton;
        annotationView.canShowCallout = YES;
    }
    else
        annotationView.annotation = annotation;
    return annotationView;
    }
    return nil;
}

- (void)mapView:(MKMapView *)mapView
 annotationView:(MKAnnotationView *)view
calloutAccessoryControlTapped:(UIControl *)control
{
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF.user.username == %@", view.annotation.title];
    NSArray *filteredArray = [self.placemarksData filteredArrayUsingPredicate:predicate];
    NSDictionary *firstFoundObject = nil;
    firstFoundObject =  filteredArray.count > 0 ? filteredArray.firstObject : nil;
    MediaViewController *mediaViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"MediaViewController_ID"];
    [self.navigationController pushViewController:mediaViewController
                                         animated:NO];
    Media *newMedia = [[Media alloc] init];
    newMedia.media_id = firstFoundObject[@"id"];
    newMedia.username = firstFoundObject[@"user"][@"username"];
    newMedia.date = firstFoundObject[@"created_time"];
    newMedia.type = firstFoundObject[@"type"];
    newMedia.url = firstFoundObject[@"images"][@"standard_resolution"][@"url"];
    newMedia.mediaImage = [[NSData alloc] initWithContentsOfURL: [NSURL URLWithString: firstFoundObject[@"images"][@"standard_resolution"][@"url"]]];
    NSDictionary *caption = firstFoundObject[@"caption"];
    if(caption != (NSDictionary *)[NSNull null])
        newMedia.caption = caption[@"text"];
    else
        newMedia.caption = @"";
    
    mediaViewController.mediaData = newMedia;
}


#pragma mark - MKMapViewDelegate
- (void)mapView:(MKMapView *)mapView didUpdateUserLocation:(MKUserLocation *)userLocation
{
    MKCoordinateRegion region = MKCoordinateRegionMakeWithDistance(userLocation.coordinate, 800, 800);
    [self.mapView setRegion:[self.mapView regionThatFits:region]
                   animated:NO];
}

#pragma mark - CLLocationManagerDelegate
- (void)locationManager:(CLLocationManager *)manager
       didFailWithError:(NSError *)error
{
    UIAlertView *errorAlert = [[UIAlertView alloc]
                               initWithTitle:@"Error"
                               message:@"Failed to Get Your Location"
                               delegate:nil
                               cancelButtonTitle:@"OK"
                               otherButtonTitles:nil];
    [errorAlert show];
}

- (void)locationManager:(CLLocationManager *)manager
    didUpdateToLocation:(CLLocation *)
newLocation fromLocation:(CLLocation *)oldLocation
{
    CLLocation *currentLocation = newLocation;
    if (currentLocation)
        [[URLRequestCenter sharedInstance] getLocationMediaWithCompletion: ^(id response) { [self processResponse: response]; }
                                                                  failure: ^(id error){ [self processError: error]; }
                                                                 latitude: [NSString stringWithFormat:@"%.8f", currentLocation.coordinate.latitude]
                                                                longitude: [NSString stringWithFormat:@"%.8f", currentLocation.coordinate.longitude] ];
    [locationManager stopUpdatingLocation];
}
@end
