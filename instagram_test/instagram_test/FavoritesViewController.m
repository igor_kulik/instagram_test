//
//  FavoritesViewController.m
//  instagram_test
//
//  Created by Igor on 4/20/15.
//  Copyright (c) 2015 Igor. All rights reserved.
//

#import "FavoritesViewController.h"
#import "URLRequestCenter.h"
#import "CDQueryCenter.h"
#import "FavoriteMediaTableViewCell.h"
#import "MediaViewController.h"
#import "UsernameTableViewController.h"
#import "Media.h"
@interface FavoritesViewController ()
@property (nonatomic, strong) NSMutableArray *tableData;

@end

@implementation FavoritesViewController
#pragma mark - View controller lifecycle
- (void)viewDidLoad
{
    [super viewDidLoad];
    segmentedControl = [[UISegmentedControl alloc] initWithItems:[NSArray arrayWithObjects:@"Media", @"Users", nil]];
    segmentedControl.frame = CGRectMake(self.view.frame.size.width / 2 - 70, self.navigationController.navigationBar.frame.size.height + 30, 140, 30);
    segmentedControl.selectedSegmentIndex = 0;
    segmentedControl.tintColor = [UIColor grayColor];
    [segmentedControl addTarget:self
                         action:@selector(valueChanged:)
               forControlEvents: UIControlEventValueChanged];
    segmentedControl.autoresizingMask = UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleBottomMargin;
    [self.view addSubview:segmentedControl];
    
    favoritesTable = [[UITableView alloc] initWithFrame: CGRectMake(self.view.frame.origin.x,
                                                                    segmentedControl.frame.origin.y + segmentedControl.frame.size.height + 5,
                                                                    self.view.frame.size.width,
                                                                    self.view.frame.size.height - self.navigationController.navigationBar.frame.size.height - segmentedControl.frame.size.height - 5)];
    favoritesTable.delegate = self;
    favoritesTable.dataSource = self;
    favoritesTable.scrollEnabled = YES;
    favoritesTable.autoresizingMask = UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleBottomMargin;
    [self.view addSubview:favoritesTable];
}
- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [self valueChanged:segmentedControl];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

#pragma mark - Segment control
- (void)valueChanged:(UISegmentedControl *)segment
{
    
    if(segment.selectedSegmentIndex == 0)
        [self loadFavoriteMedia];
    else if(segment.selectedSegmentIndex == 1)
        [self loadFavoriteUsers];
    [favoritesTable reloadData];
    [favoritesTable setContentOffset:CGPointZero animated:YES];
}

- (void) loadFavoriteMedia
{
    self.tableData = [NSMutableArray arrayWithArray:[[CDQueryCenter sharedInstance] fetchFavoriteMedia]];
}

- (void) loadFavoriteUsers
{
    self.tableData = [NSMutableArray arrayWithArray:[[CDQueryCenter sharedInstance] fetchFavoriteUsers]];
}

#pragma mark - Table view data source
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView
 numberOfRowsInSection: (NSInteger)section
{
    return [self.tableData count];
}

- (CGFloat)tableView:(UITableView *)tableView
heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return segmentedControl.selectedSegmentIndex == 0 ? 500 : 40;
}

- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(segmentedControl.selectedSegmentIndex == 0)
    {
        //Create cell
        static NSString *CellIdentifier = @"FavoriteMediaCell";
        FavoriteMediaTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier: CellIdentifier];
        if (!cell)
        {
            cell = [[FavoriteMediaTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                                     reuseIdentifier:CellIdentifier];
            [cell setFrame: CGRectMake(0, 0, favoritesTable.frame.size.width, 500)];
            //Content initialization
            cell.usernameLabel = [[UILabel alloc] init];
            cell.dateLabel = [[UILabel alloc] init];
            cell.descriptionLabel = [[UILabel alloc] init];
            cell.mediaImage = [[UIImageView alloc] init];
            //Setting frames
            cell.usernameLabel.frame = CGRectMake(cell.frame.origin.x + 10, cell.frame.origin.y + 10, cell.frame.size.width, 40);
            cell.descriptionLabel.frame = CGRectMake(cell.frame.origin.x + 10, cell.frame.size.height - 30, cell.frame.size.width - 150, 30);
            cell.dateLabel.frame = CGRectMake(cell.frame.size.width - 120, cell.frame.size.height - 30,  150, 30);
            cell.mediaImage.frame = CGRectMake(cell.frame.origin.x + 20,
                                               cell.usernameLabel.frame.origin.y + cell.usernameLabel.frame.size.height + 20,
                                               cell.frame.size.width - 40,
                                               cell.frame.size.width - 40);
            [cell.usernameLabel setFont:[UIFont systemFontOfSize:22]];
            [cell.descriptionLabel setFont:[UIFont systemFontOfSize:14]];
            [cell.dateLabel setFont:[UIFont systemFontOfSize:14]];
            //Setting autoresize properies
            cell.usernameLabel.autoresizingMask = UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleBottomMargin;
            cell.descriptionLabel.autoresizingMask = UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleBottomMargin;
            cell.dateLabel.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleBottomMargin;
            cell.mediaImage.autoresizingMask = UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleBottomMargin;
            //Adding to cell view
            [cell addSubview: cell.dateLabel];
            [cell addSubview: cell.usernameLabel];
            [cell addSubview: cell.descriptionLabel];
            [cell addSubview: cell.mediaImage];
        }
        Media *newMedia = self.tableData[indexPath.row];
        cell.descriptionLabel.text = newMedia.caption;
        cell.usernameLabel.text = newMedia.username;
        cell.dateLabel.text = newMedia.date;
        UIImage *image = [[UIImage alloc] initWithData:newMedia.mediaImage];
        [cell.mediaImage setImage: image];
        return cell;
    }
    else if(segmentedControl.selectedSegmentIndex == 1)
    {
        static NSString *CellIdentifier = @"FavoriteUserCell";
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier: CellIdentifier];
        if (!cell)
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                          reuseIdentifier:CellIdentifier];
        NSManagedObject *newUser = (NSManagedObject *)self.tableData[indexPath.row];
        NSString *text = [[newUser valueForKey:@"username"] stringByAppendingString: [NSString stringWithFormat: @": %@", [newUser valueForKey:@"full_name"]]];
        cell.textLabel.text = text;
        return cell;
    }
    return nil;
}

#pragma mark - Transitions to other screens
- (void)tableView:(UITableView *)tableView
didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(segmentedControl.selectedSegmentIndex == 0)
    {
        MediaViewController *mediaViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"MediaViewController_ID"];
        [self.navigationController pushViewController:mediaViewController
                                             animated:YES];
        mediaViewController.mediaData = [self.tableData objectAtIndex:indexPath.row];
    }
    else if(segmentedControl.selectedSegmentIndex == 1)
    {
        UsernameTableViewController *usernameViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"UsernameTableVIewController_ID"];
        [self.navigationController pushViewController:usernameViewController
                                             animated:YES];
        User *newUser = [self.tableData objectAtIndex:indexPath.row];
        usernameViewController.user_id = newUser.user_id;
    }
}
@end
