//
//  FavoriteMediaTableViewCell.m
//  instagram_test
//
//  Created by Igor on 4/20/15.
//  Copyright (c) 2015 Igor. All rights reserved.
//

#import "FavoriteMediaTableViewCell.h"

@implementation FavoriteMediaTableViewCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
