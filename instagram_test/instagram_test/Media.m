//
//  Media.m
//  instagram_test
//
//  Created by Igor on 4/22/15.
//  Copyright (c) 2015 Igor. All rights reserved.
//

#import "Media.h"

@implementation Media
//- (void) setDate: (NSString *) date
//{
//    //Date
//    NSTimeInterval interval = [date doubleValue];
//    NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
//    [dateFormatter setDateFormat:@"hh:mm dd.MM.yy"];
//    NSString *dateString = [dateFormatter stringFromDate:[NSDate dateWithTimeIntervalSince1970: interval]];
//    _date = dateString;
//}

- (NSString *) date
{
    //Date
    NSTimeInterval interval = [_date doubleValue];
    NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"hh:mm dd.MM.yy"];
    NSString *dateString = [dateFormatter stringFromDate:[NSDate dateWithTimeIntervalSince1970: interval]];
    return dateString;
}
@end
