//
//  SearchTableViewController.m
//  instagram_test
//
//  Created by Igor on 4/9/15.
//  Copyright (c) 2015 Igor. All rights reserved.
//

#import "SearchTableViewController.h"
#import "URLRequestCenter.h"
#import "UsernameTableViewController.h"
@interface SearchTableViewController ()
@property (nonatomic, strong) NSMutableArray *tableData;
@property (nonatomic, strong) NSMutableArray *searchResults;
@end

@implementation SearchTableViewController

#pragma mark - View controller lifecycle
- (void)viewDidLoad
{
    [super viewDidLoad];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

#pragma mark - UISearchBar delegate
- (void)filterContentForSearchText:(NSString*)searchText
                             scope:(NSString*)scope
{
    if (![searchText isEqual:@""])
        [[URLRequestCenter sharedInstance] searchWithCompletion: ^(id response) { [self processSearch: response]; }
                                                        failure: ^(id error){ [self processError: error]; }
                                                       username: searchText];
    else
    {
        [self.tableData removeAllObjects];
        [self.tableView reloadData];
    }
}

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    [self filterContentForSearchText:searchText scope:nil];
}

- (void) searchBarSearchButtonClicked:(UISearchBar *)theSearchBar {
    [self.searchBar resignFirstResponder];
}

#pragma mark - UIScrollView delegate
- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView {
    [self.searchBar resignFirstResponder];
}

#pragma mark - Process received data
- (void) processSearch: (NSDictionary *)searchData
{
    self.tableData = [NSMutableArray arrayWithArray: searchData[@"data"]];
    [self.tableView reloadSections:[NSIndexSet indexSetWithIndex:0] withRowAnimation:UITableViewRowAnimationAutomatic];
}

- (void) processError: (NSDictionary *)error
{
    NSString *pdata = [NSString stringWithFormat:@"error_message: %@\ncode: %@\nerror_type: %@",
                       error[@"error_message"],
                       error[@"code"],
                       error[@"error_type"]];
    
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle: @"Error"
                                                        message: pdata
                                                       delegate: self
                                              cancelButtonTitle: @"OK"
                                              otherButtonTitles: nil];
    [alertView show];
    
}

#pragma mark - Table view data source
- (NSInteger)numberOfSectionsInTableView: (UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.tableData count];
}

- (UITableViewCell *)tableView: (UITableView *)tableView
         cellForRowAtIndexPath: (NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier: CellIdentifier];
    if (!cell)
        cell = [[UITableViewCell alloc] initWithStyle: UITableViewCellStyleDefault
                                      reuseIdentifier: CellIdentifier];
    cell.accessoryType = UITableViewCellAccessoryNone;
    NSString *text = [self.tableData[indexPath.row][@"username"] stringByAppendingString: [NSString stringWithFormat: @": %@", self.tableData[indexPath.row][@"full_name"]]];
    cell.textLabel.text = text;
    return cell;
}

#pragma mark - Segue
- (void)prepareForSegue:(UIStoryboardSegue *)segue
                 sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"ShowUsername"])
    {
        NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
        UsernameTableViewController *destViewController = segue.destinationViewController;
        destViewController.user_id = self.tableData[indexPath.row][@"id"];
    }
}
@end
