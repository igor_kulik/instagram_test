//
//  ViewController.m
//  instagram_test
//
//  Created by Igor on 4/9/15.
//  Copyright (c) 2015 Igor. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    UITabBar *tabBar = self.tabBarController.tabBar;
    UITabBarItem *item = tabBar.items[self.tabBarController.selectedIndex];
    [tabBar setTintColor:[UIColor redColor]];

}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
