//
//  UsernameTableViewController.h
//  instagram_test
//
//  Created by Igor on 4/16/15.
//  Copyright (c) 2015 Igor. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UsernameTableViewController : UITableViewController
@property (strong, nonatomic) NSString *user_id;
@property (strong, nonatomic) IBOutlet UIBarButtonItem *favButton;
@property (nonatomic) BOOL isFavorite;
@end
