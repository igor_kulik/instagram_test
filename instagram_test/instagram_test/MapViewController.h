//
//  MapViewController.h
//  instagram_test
//
//  Created by Igor on 4/17/15.
//  Copyright (c) 2015 Igor. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>
#import <MapKit/MapKit.h>
#import "URLRequestCenter.h"
@interface MapViewController : UIViewController <CLLocationManagerDelegate, MKMapViewDelegate>

@end
