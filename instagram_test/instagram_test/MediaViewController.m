//
//  MediaViewController.m
//  instagram_test
//
//  Created by Igor on 4/15/15.
//  Copyright (c) 2015 Igor. All rights reserved.
//

#import "MediaViewController.h"
#import "MediaCommentCell.h"
#import "CDQueryCenter.h"
@interface MediaViewController ()
@property (nonatomic, strong) NSMutableArray *tableData;
@property (strong) NSSet *comments;
@end

@implementation MediaViewController
- (CGFloat) tableHeight
{
    return self.view.frame.size.height - self.navigationController.navigationBar.frame.size.height;
}
#pragma mark - Properties instantiation
- (NSMutableArray *) tableData
{
    if(!_tableData)
        _tableData = [[NSMutableArray alloc] initWithCapacity: 5];
    return _tableData;
}

#pragma mark - View controller lifecycle
- (void)viewDidLoad
{
    [super viewDidLoad];
//    UIImage *image = [[UIImage alloc] initWithData: self.mediaData.mediaImage];
//    self.imagePreview  = [[UIImageView alloc] initWithFrame:CGRectZero];
//    [self.imagePreview setImage: image];
//    [self.imagePreview setUserInteractionEnabled: true];
    self.view.superview.backgroundColor = self.view.backgroundColor;
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    
    [self drawContents];
}

- (void)viewDidAppear:(BOOL)animated
{
    if([[CDQueryCenter sharedInstance] checkIfMediaIsFavorite: self.mediaData.media_id])
    {
        self.isFavorite = YES;
        self.tableData = [[[[CDQueryCenter sharedInstance] fetchComments:self.mediaData] allObjects] mutableCopy];
        [self.favButton setImage:[UIImage imageNamed:@"fav"]];
    }
    else
    {
        self.isFavorite = NO;
        [self.favButton setImage:[UIImage imageNamed:@"fav_empty"]];
    }
        [self.commentsTableView reloadData];
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{

    CGRect headerRect = [tableView rectForHeaderInSection:0];
    UIView *headerView = [[UIView alloc] initWithFrame: headerRect];
   
    self.scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0,
                                                                     0,
                                                                     headerRect.size.height,
                                                                     headerRect.size.height)];
    self.scrollView.backgroundColor = self.view.backgroundColor;
    self.scrollView.delegate = self;
    self.scrollView.scrollEnabled = YES;
    self.scrollView.minimumZoomScale = 1.0f;
    self.scrollView.maximumZoomScale = 8.0f;
    
    self.imagePreview  = [[UIImageView alloc] initWithFrame: CGRectMake(0,
                                                                        0,
                                                                        headerRect.size.height,
                                                                        headerRect.size.height)];
    UIImage *image = [[UIImage alloc] initWithData: self.mediaData.mediaImage];
    self.imagePreview.image = image;
    self.imagePreview.contentMode = UIViewContentModeScaleAspectFit;
    self.imagePreview.userInteractionEnabled = YES;
    [self.imagePreview addGestureRecognizer: [[UITapGestureRecognizer alloc] initWithTarget:self
                                                                                     action:@selector(tapOnPreview:)]];

    self.scrollView.center = headerView.center;
    [self.scrollView addSubview:self.imagePreview];

    headerView.backgroundColor = self.view.backgroundColor;
    [headerView addSubview:self.scrollView];
    return headerView;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    CGRect footerRect = [tableView rectForFooterInSection:0];
    UIView *footerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.commentsTableView.frame.size.width, footerRect.size.height)];
    
    self.commentTextField  = [[UITextField alloc] initWithFrame:CGRectMake(0,
                                                                           footerView.frame.size.height * 0.1,
                                                                           footerView.frame.size.width - footerView.frame.size.height * 0.8,
                                                                           footerView.frame.size.height * 0.8)];
    self.commentTextField.adjustsFontSizeToFitWidth = YES;
    self.commentTextField.minimumFontSize = 1.0;
    if(self.isFavorite)
    {
        self.commentTextField.clearsOnBeginEditing = YES;
        self.commentTextField.clearButtonMode = UITextFieldViewModeWhileEditing;
        self.commentTextField.borderStyle = UITextBorderStyleRoundedRect;
        self.commentTextField.delegate = self;
        self.commentTextField.text = @"add comment...";
    
        self.addCommentButton = [[UIButton alloc] initWithFrame:CGRectMake(footerView.frame.size.width - footerView.frame.size.height * 0.8,
                                                                           footerView.frame.size.height * 0.1,
                                                                           footerView.frame.size.height * 0.8,
                                                                           footerView.frame.size.height * 0.8)];
        [self.addCommentButton setBackgroundImage:[UIImage imageNamed:@"addIcon"] forState:UIControlStateNormal];
        [self.addCommentButton addTarget: self
                                  action: @selector(addCommentButtonClicked:)
                        forControlEvents: UIControlEventTouchUpInside];
        
        [footerView addSubview:self.addCommentButton];
    }
    else
    {
        CGRect frame = self.commentTextField.frame;
        frame.size.width = footerView.frame.size.width;
        self.commentTextField.frame = frame;
        self.commentTextField.userInteractionEnabled = NO;
        self.commentTextField.text = @"Comments are allowed only for favorites";
        self.commentTextField.textAlignment = NSTextAlignmentCenter;
    }
    footerView.backgroundColor = self.view.backgroundColor;

    [footerView addSubview:self.commentTextField];
    return footerView;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return [self tableHeight] * 0.6;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return [self tableHeight] * 0.1;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return [self tableHeight] * 0.08;
}

- (void) drawContents
{
    CGFloat borderOffset = 0.05;
    self.commentsTableView.frame = CGRectMake(self.view.frame.size.width * borderOffset,
                                              self.view.frame.origin.y,
                                              self.view.frame.size.width - self.view.frame.size.width * 2 * borderOffset,
                                              self.view.frame.size.height - self.view.frame.size.height / 2 * borderOffset);
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

#pragma mark - Image and video processing
- (void)playVideo:(NSString *) videoURL
{
    _moviePlayerController = [[MPMoviePlayerViewController alloc] initWithContentURL:[NSURL URLWithString:videoURL]];
    [_moviePlayerController.moviePlayer setFullscreen: true];
    [_moviePlayerController.moviePlayer setShouldAutoplay: true];
    [_moviePlayerController.moviePlayer setControlStyle: MPMovieControlStyleFullscreen];
    [_moviePlayerController.moviePlayer prepareToPlay];
    [self presentMoviePlayerViewControllerAnimated: _moviePlayerController];
}

- (void)showImage
{
    [self.scrollView addGestureRecognizer: [[UIPinchGestureRecognizer alloc] initWithTarget:self
                                                                                     action:@selector(pinchOnView:)]];
}

- (UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView
{
    return _scrollView.subviews.firstObject;
}

- (void)scrollViewDidEndZooming:(UIScrollView *)scrollView withView:(UIView *)view atScale:(CGFloat)scale
{
    
}

#pragma mark - Gesture recognizing
- (IBAction)tapOnPreview:(UITapGestureRecognizer *)sender
{
    if (sender.state == UIGestureRecognizerStateEnded)
    {
        if([self.mediaData.type isEqualToString: @"video"])
            [self playVideo: self.mediaData.url];
        else if ([self.mediaData.type isEqualToString: @"image"])
            [self showImage];
    }
}

- (void)pinchOnView:(UIPinchGestureRecognizer *)gesture
{
    if (gesture.state == UIGestureRecognizerStateEnded ||
        gesture.state == UIGestureRecognizerStateChanged)
    {
        self.scrollView.zoomScale *= gesture.scale;
        gesture.scale = 1.0;
    }
}

#pragma mark - Add to favorites in core data
- (IBAction)favButtonClicked:(id)sender
{
    if(self.isFavorite)
        [[CDQueryCenter sharedInstance] removeMediaFromFavorites: self.mediaData];
    else
        [[CDQueryCenter sharedInstance] addMediaToFavorites: self.mediaData];
    [self viewDidAppear:YES];
}

#pragma mark - Comments processing
- (IBAction)removeCommentButtonClicked:(UIButton *)sender
{
    UITableViewCell *cell = (UITableViewCell *)sender.superview.superview;
    NSIndexPath *indexPath = [(UITableView *)self.commentsTableView indexPathForCell: cell];
    [[CDQueryCenter sharedInstance] deleteComment:[self.tableData objectAtIndex:indexPath.row]];
    [self dismissViewControllerAnimated:YES completion:nil];
    [self viewDidAppear:YES];

}

- (IBAction)addCommentButtonClicked:(UIButton *)sender
{
    if (![self.commentTextField.text isEqualToString: @"add comment..."] &&
        ![self.commentTextField.text isEqualToString: @""])
    {
        if(self.isFavorite)
        {
            Comment *newComment = [[Comment alloc]init];
            newComment.text = self.commentTextField.text;
            [[CDQueryCenter sharedInstance] addComment:newComment
                                              forMedia:self.mediaData];
            [self.commentTextField setText: @""];
        }
        else
            NSLog(@"error, media is not in favorites");
    }
    [self viewDidAppear:YES];
}

#pragma mark - Table view data source
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView
 numberOfRowsInSection: (NSInteger)section
{
    return [self.tableData count];
}

- (MediaCommentCell *)tableView:(UITableView *)tableView
          cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"MediaCommentCell_ID";
    MediaCommentCell *cell = [tableView dequeueReusableCellWithIdentifier: CellIdentifier];
    if (!cell)
        cell = [[MediaCommentCell alloc] initWithStyle:UITableViewCellStyleDefault
                                       reuseIdentifier:CellIdentifier];
    NSManagedObject *comment = [self.tableData objectAtIndex:indexPath.row];
    [cell.commentLabel setText:[NSString stringWithFormat:@"%@", [comment valueForKey:@"text"]]];
    return cell;
}

#pragma mark - UITextField delegate
- (BOOL)textFieldShouldEndEditing:(UITextField *)textField {
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
    
    [self.view endEditing:YES];
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    
    return YES;
}

#pragma mark - Keyboard notifications
- (void)keyboardWillShow:(NSNotification *)notification
{
    // Assign new frame to your view
    NSDictionary* info = [notification userInfo];
    CGSize kbSize = [[info objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue].size;
    [self.view setFrame:CGRectMake(0,
                                   self.tabBarController.tabBar.frame.size.height - kbSize.height,
                                   self.view.superview.frame.size.width,
                                   self.view.frame.size.height)];
}

-(void)keyboardWillHide:(NSNotification *)notification
{
    [self.view setFrame:CGRectMake(0,
                                   self.view.superview.frame.origin.y,
                                   self.view.superview.frame.size.width,
                                   self.view.frame.size.height)];

}
@end
