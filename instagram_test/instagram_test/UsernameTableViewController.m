//
//  UsernameTableViewController.m
//  instagram_test
//
//  Created by Igor on 4/16/15.
//  Copyright (c) 2015 Igor. All rights reserved.
//

#import "UsernameTableViewController.h"
#import "URLRequestCenter.h"
#import "CDQueryCenter.h"
#import "FeedTableViewCell.h"
#import "MediaViewController.h"
#import "User.h"
@interface UsernameTableViewController ()
@property (nonatomic, strong) NSArray *tableData;
@end

@implementation UsernameTableViewController
UIAlertView *alertView;
#pragma mark - View controller lifecycle
- (void)viewDidLoad
{
    [super viewDidLoad];
    [[URLRequestCenter sharedInstance] getUserRecentWithCompletion: ^(id response) { [self processFeed: response]; }
                                                           failure: ^(id error){ [self processError: error]; }
                                                           user_id: self.user_id];
}

- (void)viewDidAppear:(BOOL)animated
{

    if([[CDQueryCenter sharedInstance] checkIfUserIsFavorite: self.user_id])
    {
        self.isFavorite = YES;
        [self.favButton setImage:[UIImage imageNamed:@"fav"]];
    }
    else
    {
        self.isFavorite = NO;
        [self.favButton setImage:[UIImage imageNamed:@"fav_empty"]];
    }
    [self.tableView reloadData];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

#pragma mark - Process received data
- (void) processFeed: (NSDictionary *)feedData
{
    self.tableData = [NSArray arrayWithArray: feedData[@"data"]];
    [self.tableView reloadData];
}

- (void) processError: (NSDictionary *)error
{
    if([error[@"code"] longValue] == 400)
    {
        alertView = [[UIAlertView alloc] initWithTitle: @"Privacy error"
                                               message: @"Only user's friends have access to his media"
                                              delegate: self
                                     cancelButtonTitle: @"OK"
                                     otherButtonTitles: nil];
    }
    else
    {
        NSString *pdata = [NSString stringWithFormat:@"error_message: %@\ncode: %@\nerror_type: %@",
                           error[@"error_message"],
                           error[@"code"],
                           error[@"error_type"]];
        alertView = [[UIAlertView alloc] initWithTitle: @"Error"
                                               message: pdata
                                              delegate: self
                                     cancelButtonTitle: @"OK"
                                     otherButtonTitles: nil];
    }
    [alertView show];
}

#pragma mark - Table view data source
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView
 numberOfRowsInSection: (NSInteger)section
{
    return [self.tableData count];
}

- (FeedTableViewCell *)tableView:(UITableView *)tableView
           cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    //Create cell
    static NSString *CellIdentifier = @"UsernameTableCell";
    FeedTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier: CellIdentifier];
    if (!cell)
        cell = [[FeedTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    [cell.usernameLabel setHidden:true];
    //Configure the cell
    //Description
    NSDictionary *caption = self.tableData[indexPath.row][@"caption"];
    if(caption != (NSDictionary *)[NSNull null])
        cell.descriptionLabel.text = caption[@"text"];
    else
        cell.descriptionLabel.text = @"";
    //Date
    NSTimeInterval interval = [self.tableData[indexPath.row][@"created_time"] doubleValue];
    NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"hh:mm dd.MM.yy"];
    NSString *dateString = [dateFormatter stringFromDate:[NSDate dateWithTimeIntervalSince1970:interval]];
    cell.dateLabel.text = dateString;
    //Image
    NSData *data = [[NSData alloc] initWithContentsOfURL:[NSURL URLWithString: self.tableData[indexPath.row][@"images"][@"standard_resolution"][@"url"]]];
    UIImage *image = [[UIImage alloc] initWithData:data];
    [cell.mediaImage setImage:image];
    cell.accessoryType = UITableViewCellAccessoryNone;

    return cell;
}

#pragma mark - Add to favorites in core data
- (IBAction)favButtonClicked:(id)sender
{
    NSDictionary *userData = [NSDictionary dictionaryWithDictionary:self.tableData[0][@"user"]];
    User *newUser = [[User alloc]init];
    newUser.username = userData[@"username"];
    newUser.full_name = userData[@"full_name"];
    newUser.user_id = userData[@"id"];
    if(self.isFavorite)
        [[CDQueryCenter sharedInstance] removeUserFromFavorites: newUser];
    else
        [[CDQueryCenter sharedInstance] addUserToFavorites: newUser];
    [self viewDidAppear:NO];
}

#pragma mark - Transition to media screen
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    MediaViewController *mediaViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"MediaViewController_ID"];
    [self.navigationController pushViewController:mediaViewController
                                         animated:YES];
    
    NSDictionary *media = [self.tableData objectAtIndex:indexPath.row];
    Media *newMedia = [[Media alloc] init];
    newMedia.media_id = media[@"id"];
    newMedia.username = media[@"user"][@"username"];
    newMedia.date = media[@"created_time"];
    newMedia.type = media[@"type"];
    NSString *mediaDirectory = [newMedia.type isEqualToString:@"image"] ? @"images": @"videos";
    newMedia.url = media[mediaDirectory][@"standard_resolution"][@"url"];
    newMedia.mediaImage = [[NSData alloc] initWithContentsOfURL: [NSURL URLWithString: media[@"images"][@"standard_resolution"][@"url"]]];
    NSDictionary *caption = media[@"caption"];
    if(caption != (NSDictionary *)[NSNull null])
        newMedia.caption = caption[@"text"];
    else
        newMedia.caption = @"";

    
    mediaViewController.mediaData = newMedia;
}
@end
