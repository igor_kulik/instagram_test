//
//  URLRequestCenter.h
//  instagramTest
//
//  Created by Igor on 4/9/15.
//  Copyright (c) 2015 Igor. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface URLRequestCenter : NSObject<NSURLConnectionDelegate>
{
    NSMutableData *_responseData;
}
@property (copy) void (^completion)(id);
@property (copy) void (^failure)(id);
- (void) getFeedWithCompletion: (void (^)(id response)) completed
                       failure: (void (^)(id error)) failed;
- (void) searchWithCompletion: (void (^)(id response)) completed
                      failure: (void (^)(id error)) failed
                     username: (NSString *)username;
- (void) getLocationMediaWithCompletion: (void (^)(id response)) completed
                                failure: (void (^)(id error)) failed
                               latitude: (NSString *)lat
                              longitude: (NSString *)lng;
- (void) getUserRecentWithCompletion: (void (^)(id response)) completed
                             failure: (void (^)(id error)) failed
                             user_id: (NSString *)user_id;
+ (URLRequestCenter *) sharedInstance;
@end