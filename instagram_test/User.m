//
//  User.m
//  instagram_test
//
//  Created by Igor on 4/22/15.
//  Copyright (c) 2015 Igor. All rights reserved.
//

#import "User.h"


@implementation User

@dynamic username;
@dynamic full_name;
@dynamic user_id;
@dynamic usersMedia;

@end
