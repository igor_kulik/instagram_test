//
//  URLRequestCenter.m
//  instagramTest
//
//  Created by Igor on 4/9/15.
//  Copyright (c) 2015 Igor. All rights reserved.
//

#import "URLRequestCenter.h"
#import "JSON.h"
@implementation URLRequestCenter
static URLRequestCenter *sSingleton = nil;
NSString *client_ID = @"f9abf80f193343dfafd665d879ffe8e2";

#pragma mark - Feed
- (void) getFeedWithCompletion: (void (^)(id response)) completed
                       failure: (void (^)(id error))failed
{
    self.failure = failed;
    self.completion = completed;
    NSURLRequest * feedRequest = [NSURLRequest requestWithURL:
                                  [NSURL URLWithString:
                                   [NSString stringWithFormat:@"https://api.instagram.com/v1/media/popular?client_id=%@", client_ID]]];
    [self sendURLRequest:feedRequest];
}

#pragma mark - Search
- (void) searchWithCompletion: (void (^)(id response)) completed
                      failure: (void (^)(id error))failed
                     username: (NSString *)username
{
    self.failure = failed;
    self.completion = completed;
    NSURLRequest * searchRequest = [NSURLRequest requestWithURL:
                                    [NSURL URLWithString:
                                     [NSString stringWithFormat:@"https://api.instagram.com/v1/users/search?q=%@&client_id=%@", username, client_ID]]];
    [self sendURLRequest:searchRequest];
}

#pragma mark - Location media
- (void) getLocationMediaWithCompletion: (void (^)(id response)) completed
                                failure: (void (^)(id error)) failed
                               latitude: (NSString *)lat
                              longitude: (NSString *)lng
{
    self.failure = failed;
    self.completion = completed;
    NSURLRequest * locationRequest = [NSURLRequest requestWithURL:
                                      [NSURL URLWithString:
                                       [NSString stringWithFormat:@"https://api.instagram.com/v1/media/search?lat=%@&lng=%@&client_id=%@",lat, lng, client_ID]]];
    [self sendURLRequest:locationRequest];
}

#pragma mark - User feed
- (void) getUserRecentWithCompletion: (void (^)(id response)) completed
                             failure: (void (^)(id error)) failed
                             user_id: (NSString *)user_id
{
    self.failure = failed;
    self.completion = completed;
    NSURLRequest * userFeedRequest = [NSURLRequest requestWithURL:
                                    [NSURL URLWithString:
                                     [NSString stringWithFormat:@"https://api.instagram.com/v1/users/%@/media/recent?client_id=%@", user_id, client_ID]]];
    [self sendURLRequest:userFeedRequest];
}

#pragma mark - Connection
- (void) sendURLRequest: (NSURLRequest *)request
{
    (void)[[NSURLConnection alloc]  initWithRequest:request
                                           delegate:self
                                   startImmediately:true];
}

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
    _responseData = [[NSMutableData alloc] init];
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    [_responseData appendData:data];
}

- (NSCachedURLResponse *)connection:(NSURLConnection *)connection
                  willCacheResponse:(NSCachedURLResponse*)cachedResponse
{
    return nil;
}

#pragma mark - Parsing
- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    NSString *response = [[NSString alloc] initWithData: _responseData
                                               encoding: NSUTF8StringEncoding];
    SBJsonParser *jResponse = [[SBJsonParser alloc]init];
    NSDictionary *tokenData = [jResponse objectWithString:response];
    if ([tokenData objectForKey: @"data"])
        self.completion(tokenData);
    else
        self.failure(tokenData[@"meta"]);
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    self.failure(error);
}

#pragma mark - Singleton properties
+ (URLRequestCenter *) sharedInstance
{
    @synchronized(self)
    {
        if(!sSingleton)
            sSingleton = [[super allocWithZone:NULL] init];
    }
    return sSingleton;
}

+ (id) allocWithZone:(NSZone *)zone
{
    return [[self sharedInstance] retain];
}

- (id) copyWithZone:(NSZone*)zone
{
    return self;
}

- (id) retain
{
    return self;
}

- (NSUInteger) retainCount
{
    return NSUIntegerMax;
}

- (void) release { }

- (id) autorelease
{
    return self;
}
@end
